// Author Talha Arslan

#include <bits/stdc++.h>

using namespace std;
int depth = 3;
bool isPrime(int x){
    for(int i = 2; i <= sqrt(x); i++){
        if(x%i == 0)
            return 0;    
    }
    return 1;
}
struct Point{
    int x,y,cost,prevx,prevy;
    

};
struct Node{
    int cost,node,previousX,previousY;
    

};
struct Node a[1000][1000];
void mapp()
{
    for (int i=0; i <=depth; i++){
        for(int j=0 ; j <= i; j++){
            cout << a[i][j].cost << " ";

        }
        cout << endl;
    }
}
void back(int x, int y)
{
    cout << endl << y << " " << x;
    if(y == 0)
        return;
   
    back(a[y][x].previousX,a[y][x].previousY);
}

queue <struct Point> q;
int main(){
    for (int i=0; i <=depth; i++){
        for(int j=0 ; j <= i; j++){
            cin >> a[i][j].node;
        }
    }
    a[0][0].cost = a[0][0].node;
    // mapp();
    struct Point tmp;
    tmp.x = 0;
    tmp.y = 0;
    tmp.cost = a[0][0].node;
    q.push(tmp);
    while(!q.empty())
    {

        tmp = q.front();
        q.pop();
        
        
        if(tmp.y == depth + 1)
            continue;
       
        if(a[tmp.y][tmp.x].cost > tmp.cost)
            continue;
        else if(a[tmp.y][tmp.x].cost <= tmp.cost) 
       {  
           // cout << tmp.y << " " << tmp.x << " " << tmp.cost <<endl;
            a[tmp.y][tmp.x].previousX = tmp.prevx;
            a[tmp.y][tmp.x].previousY = tmp.prevy;
            a[tmp.y][tmp.x].cost = tmp.cost;
            struct Point add;
            add.prevx = tmp.x;
            add.prevy = tmp.y;
            add.x = tmp.x;
            add.y = tmp.y + 1 ;
            add.cost = tmp.cost + a[add.y][add.x].node;
            if(!isPrime(a[add.y][add.x].node))
                q.push(add);
            
            add.x = tmp.x + 1;
            add.y = tmp.y + 1 ;
            add.cost = tmp.cost + a[add.y][add.x].node;
            if(!isPrime(a[add.y][add.x].node))
                q.push(add);
        }
        //1 8 4 2 6 9 8 5 9 3
    }
    mapp();
    tmp.cost = -999999;
    tmp.x = -1;
    tmp.y = -1; 
    for(int i = 0 ; i <= depth; i ++)
    {
        if(a[depth][i].cost  >= tmp.cost)
        {
            tmp.cost = a[depth][i].cost;
            tmp.x = i;
            tmp.y = depth;
        }
        //maxi = max(maxi,a[depth][i]);
    }
    cout << "the biggest sum : " << tmp.cost << " " << tmp.y<< " "<< tmp.x;
    back(tmp.x,tmp.y);
    return 0;
}